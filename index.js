﻿const express = require("express");
const app = express();
const morgan = require("morgan");
const bodyParser = require("body-parser");
const jwt = require("./lib/auth/jwt");
const errorHandler = require("./lib/handlers/error");
const usersRoute = require("./lib/routes/users");
const config = require("./config");

app.use(morgan("dev"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "PUT, POST, DELETE, GET");
    return res.status(200).json({});
  }
  next();
});

// use JWT auth to secure the api
app.use(jwt());

// api routes
app.use("/users", usersRoute);

// global error handler
app.use(errorHandler);

// start server
app.listen(config.port, function() {
  console.log("Server listening on port " + config.port);
});
