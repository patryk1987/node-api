﻿const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");

let login = (req, res, next) => {
  userController.authenticate(req, res, next);
};

let register = (req, res, next) => {
  userController.create(req, res, next);
};

let getCurrent = (req, res, next) => {
  userController.getByIdToken(req, res, next);
};

let getById = (req, res, next) => {
  userController.getByIdRequest(req, res, next);
};

let update = (req, res, next) => {
  userController.update(req, res, next);
};

let _delete = (req, res, next) => {
  userController.delete(req, res, next);
};

// routes
router.post("/login", login);
router.post("/", register);
router.get("/", getCurrent);
router.get("/:id", getById);
router.put("/:id", update);
router.delete("/:id", _delete);

module.exports = router;
