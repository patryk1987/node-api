const expressJwt = require("express-jwt");
const config = require("../../config");
const userController = require("../controllers/user");

let jwt = function() {
  const secret = config.secret;
  return expressJwt({ secret, isRevoked }).unless({
    path: [
      // public routes that don't require authentication
      "/users/login",
      { url: "/users", methods: ["POST"] }
    ]
  });
};

let isRevoked = async (req, payload, done) => {
  const user = await userController.getByIdVerify(payload.sub);

  // revoke token if user no longer exists
  if (!user) {
    return done(null, true);
  }

  done();
};

module.exports = jwt;
