﻿const config = require("../../config");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const db = require("../inits/db");
const User = db.User;

let authenticate = (req, res, next) => {
  const { email, password } = req.body;
  User.findOne({ email })
    .then(user => {
      if (user && bcrypt.compareSync(password, user.hash)) {
        const { hash, ...userWithoutHash } = user.toObject();
        const token = jwt.sign({ sub: user.id }, config.secret);
        res.json({
          ...userWithoutHash,
          token
        });
      } else {
        res.status(400).json({ message: "Email or password is incorrect" });
      }
    })
    .catch(err => next(err));
};

let getByIdVerify = async id => {
  return await User.findById(id).select("-hash");
};

let getById = (id, res, next) => {
  User.findById(id)
    .select("-hash")
    .then(user => {
      if (user) {
        res.json(user);
      } else {
        res.sendStatus(404);
      }
    })
    .catch(err => next(err));
};

let getByIdRequest = (req, res, next) => {
  getById(req.params.id, res, next);
};

let getByIdToken = (req, res, next) => {
  getById(req.user.sub, res, next);
};

let create = async (req, res, next) => {
  const userParam = req.body;

  // validate
  if (await User.findOne({ email: userParam.email })) {
    return next('Email "' + userParam.email + '" is already taken');
  }

  const user = new User(userParam);

  // hash password
  if (userParam.password) {
    user.hash = bcrypt.hashSync(userParam.password, 10);
  }

  // save user
  user
    .save()
    .then(() => res.json({}))
    .catch(err => next(err));
};

let update = async (req, res, next) => {
  const id = req.params.id;
  const userParam = req.body;
  const user = await User.findById(id);

  // validate
  if (!user) return next("User not found");
  if (
    user.email !== userParam.email &&
    (await User.findOne({ email: userParam.email }))
  ) {
    return next('Email "' + userParam.email + '" is already taken');
  }

  // hash password if it was entered
  if (userParam.password) {
    userParam.hash = bcrypt.hashSync(userParam.password, 10);
  }

  // copy userParam properties to user
  Object.assign(user, userParam);

  user
    .save()
    .then(() => res.json({}))
    .catch(err => next(err));
};

let _delete = (req, res, next) => {
  const id = req.params.id;
  User.findByIdAndRemove(id)
    .then(user => {
      if (user) {
        res.json({});
      } else {
        throw 'User "' + id + "\" does'nt exist";
      }
    })
    .catch(err => next(err));
};

module.exports = {
  authenticate,
  getByIdVerify,
  getByIdRequest,
  getByIdToken,
  create,
  update,
  delete: _delete
};
