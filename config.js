﻿module.exports = {
  port: process.env.NODE_ENV === "production" ? 80 : 8080,
  connectionString: "mongodb://localhost:27017/myApp",
  secret: "p1M83#z*281BFQ!@&o2m9z0b3"
};
